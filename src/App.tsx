import React from 'react';
import { Route } from 'react-router-dom';
import { Router } from 'react-router';
// @ts-ignore
import { AnimatedSwitch } from 'react-router-transition';

import { createBrowserHistory } from 'history';

import { ThemeProvider } from '@material-ui/core';

import appRoutes from './app/app.routes';
import theme from './app/Core/Theme';

import './App.css';
import './assets/styles/styles.scss';

const history = createBrowserHistory();

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <AnimatedSwitch
          atEnter={{ opacity: 0 }}
          atLeave={{ opacity: 0 }}
          atActive={{ opacity: 1 }}
          className="switch-wrapper"
        >
          {appRoutes.map(({ component, exact, path }, i) => (
            <Route path={path} key={i} exact={exact}>
              {component}
            </Route>
          ))}
        </AnimatedSwitch>
      </Router>
    </ThemeProvider>
  );
};

export default App;
