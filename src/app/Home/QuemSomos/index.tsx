import React from "react";

import Anchor from "../Anchor";

import banner from "../../../assets/images/section4/banner-porque-escolher-o-adiante-mobile.png";

import "./styles.scss";
import { Link } from "react-router-dom";

const QuemSomos = () => {
  return (
    <section className="section4 has-anchor">
      <Anchor anchorId="quem-somos" />

      <div className="wrapper">
        <div className="left">
          <div className="wrapper">
            <h4 className="white">Quem somos</h4>
            <h1 className="white">Por que escolher o Adiante?</h1>

            <p className="content">
              Nossa missão é facilitar o processo de antecipação das notas
              fiscais de todas as empresas que precisam de ajuda financeira.
              Nosso intuito é trazer mais praticidade e rapidez para o seu
              negócio, com um sistema 100% online e 100% seguro.
            </p>

            <p>
              <strong>Queremos a sua empresa indo Adiante!</strong>
            </p>

            <Link to="/cadastro" className="button-outline-white big">
              QUERO ANTECIPAR
            </Link>
          </div>
        </div>

        <div className="right">
          <img src={banner} alt="Adiante banner por que o Adiante" />
        </div>
      </div>
    </section>
  );
};

export default QuemSomos;
