import React, { useState } from "react";
import useForm from "react-hook-form";

import CustomSnackbar from "../../Core/Components/CustomSnackbar";

import Loading from "../../Core/Components/Loading";
import { regexEmail } from "../../Core/Utils/regex";

import addy from "../../../assets/images/section7/addy-masconte-adiante.svg";
import addyMobile from "../../../assets/images/section7/addy-masconte-adiante-mobile.svg";

import "./styles.scss";

type FormData = {
  email: string;
};

const NewsAdiante: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage] = useState("Cadastro efetuado com sucesso.");
  const [snackbarStyle] = useState("success");
  const { errors, register, handleSubmit } = useForm<FormData>();

  const onSubmit = handleSubmit(data => {
    const { email } = data;

    setLoading(true);

    const campos = {
      token_api_lahar:
        "viewtechbubs75MbftdIZZpt1WUT8WwqaUxjqiw5GVhRKXqWYlNKHTUgzEyeFJon",
      nome_formulario: "Newsletter Adiante",
      url_origem: window.location.href,
      email_contato: email
    };

    const inputs = document.getElementsByClassName("api-lahar");
    const elementos = Array.from(inputs);

    // @ts-ignore
    integracao_js(campos, "redireciona", elementos, "conversions");

    setTimeout(() => {
      setLoading(false);
      setSnackbarOpened(true);
    }, 2000);
  });

  return (
    <section className="section7">
      <Loading isLoading={loading} />

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle={snackbarStyle}
      />

      <div className="container">
        <div className="left">
          <div className="headings">
            <h2>News Adiante</h2>
            <h1>Quer saber mais sobre antecipação de recebíveis?</h1>
          </div>

          <div className="input-container">
            <form id="form" onSubmit={onSubmit}>
              <input
                className="simple-input api-lahar lahar_email_contato"
                type="email"
                placeholder="E-mail"
                name="email"
                ref={register({
                  required: {
                    message: "E-mail inválido",
                    value: true
                  },
                  pattern: {
                    value: regexEmail,
                    message: "E-mail inválido"
                  }
                })}
              />

              <span className="error-message">{errors.email?.message}</span>

              <button className="button-flat-primary big large" type="submit">
                CADASTRAR
              </button>
            </form>
          </div>
        </div>

        <div className="right">
          <img
            src={addy}
            alt="Addy - Mascote Blim - Conte com a fintech mais rápida do mundo!"
          />

          <h4>
            Conte com a <strong>fintech</strong> mais rápida do mundo!
          </h4>

          <img
            src={addyMobile}
            alt="Addy - Mascote Blim - Conte com a fintech mais rápida do mundo!"
            className="addy-mobile"
          />
        </div>
      </div>
    </section>
  );
};

export default NewsAdiante;
