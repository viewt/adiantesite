import React from "react";
import { Link } from "react-router-dom";

import Anchor from "../Anchor";

import terceirizacao from "../../../assets/images/section3/terceirizacao-do-setor.svg";
import organizacao from "../../../assets/images/section3/organizacao.svg";
import liberacao from "../../../assets/images/section3/liberacao-rapida.svg";
import menorRisco from "../../../assets/images/section3/menor-risco.svg";
import recursos from "../../../assets/images/section3/recursos.svg";
import taxas from "../../../assets/images/section3/taxas-competitivas.svg";
import banner from "../../../assets/images/section3/adiante-banner-vantagens-de-adiantar.svg";

import "./styles.scss";

const Vantagens = () => {
  return (
    <section className="section3">
      <div className="container">
        <div className="left has-anchor">
          <Anchor anchorId="vantagens" offset={80} />

          <h5 className="light">Adiantamento</h5>

          <h2 className="gray">Vantagens de adiantar</h2>

          <p>
            A antecipação de duplicatas é a opção mais vantajosa para a sua
            empresa, pois, comparada ao empréstimo empresarial, possui taxas bem
            mais competitivas.
            <br />
            <strong>
              O Adiante é a fintech com melhores condições no mercado!
            </strong>
          </p>

          <div className="items">
            <div className="item">
              <img
                src={terceirizacao}
                alt="Terceirização do setor de crédito e cobrança"
              />

              <p>
                <strong>Terceirização do setor</strong> de crédito e cobrança
              </p>
            </div>

            <div className="item">
              <img
                src={organizacao}
                alt="Organização imediata de fluxo de caixa"
              />

              <p>
                <strong>Organização</strong> imediata do fluxo de caixa
              </p>
            </div>

            <div className="item">
              <img src={liberacao} alt="Liberação rápida" />

              <p>
                <strong>Liberação rápida,</strong> funcional e 100% digital
              </p>
            </div>

            <div className="item">
              <img src={menorRisco} alt="Menor risco de inadimplência" />

              <p>
                <strong>Menor risco</strong> de inadimplência dos seus clientes
              </p>
            </div>

            <div className="item">
              <img src={recursos} alt="Recursos para a sua empresa" />

              <p>
                <strong>Recursos</strong> para a sua empresa, sem gerar dívidas
              </p>
            </div>

            <div className="item">
              <img src={taxas} alt="Taxas competitivas" />

              <p>
                <strong>Taxas competitivas</strong> e sem cobrança de tarifa
              </p>
            </div>
          </div>

          <Link to="/cadastro" className="button-flat-primary">
            QUERO ANTECIPAR
          </Link>
        </div>

        <div className="right">
          <img src={banner} alt="Vantagens de Adiantar - Adiante Banner" />
        </div>
      </div>
    </section>
  );
};

export default Vantagens;
