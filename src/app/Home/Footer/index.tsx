import React from "react";

import { Link } from "react-router-dom";

import facebook from "../../../assets/images/footer/facebook.svg";
import gcb from "../../../assets/images/footer/grupo-gcb.svg";
import googlePlay from "../../../assets/images/footer/google-play.svg";
import instagram from "../../../assets/images/footer/instagram.svg";
import linkedin from "../../../assets/images/footer/linkedin.svg";
import logo from "../../../assets/images/adiante-logo-white.svg";

import "./styles.scss";

const Footer = () => {
  return (
    <section className="footer">
      <div className="wrapper">
        <div className="top">
          <img src={logo} alt="Logo Adiante Branco" />
        </div>

        <div className="items">
          <div className="item company-info">
            <div className="item-content">
              <span>
                <strong>LWM Corporate Center Torre A - 9º Andar</strong>
              </span>
              <span>Rua George Ohm, 206 - CEP 04576-020</span>
              <span>São Paulo - SP, Cidade Monções</span>
              <span>CNPJ: 33.013.052/0001-51</span>
            </div>
          </div>

          <nav className="item nav">
            <div className="item-content">
              <Link to="/">
                <strong>Início</strong>
              </Link>

              <a href="#como-fazer">Como fazer</a>
              <a href="#vantagens">Vantagens</a>
              <a href="#quem-somos">Quem somos</a>
              <a href="#faq">Perguntas Frequentes</a>
            </div>
          </nav>

          <div className="item grupo-gcb">
            <div className="item-content">
              <a
                href="https://gcbinvestimentos.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={gcb} alt="Grupo GCB" />
              </a>
            </div>
          </div>

          <div className="item contact">
            <div className="item-content">
              <h4>Nossas Redes Sociais</h4>

              <div className="social-links">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.linkedin.com/company/adiante-s-a"
                >
                  <img src={linkedin} alt="linkedin" />
                </a>

                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.facebook.com/adianteapp"
                >
                  <img src={facebook} alt="facebook" />
                </a>

                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.instagram.com/adiante.app"
                >
                  <img src={instagram} alt="instagram" />
                </a>
              </div>

              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://play.google.com/store/apps/details?id=com.adiantesa&hl=pt"
              >
                <img
                  src={googlePlay}
                  alt="Google Play Botão"
                  className="google-play"
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
