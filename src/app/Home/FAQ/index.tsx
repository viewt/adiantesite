import React from "react";

import Anchor from "../Anchor";

import banner from "../../../assets/images/section6/banner-adiante-faq.svg";

import "./styles.scss";

const FAQ = () => {
  return (
    <section className="section6 has-anchor">
      <Anchor anchorId="faq" />

      <div className="wrapper">
        <div className="left">
          <h2>FAQ</h2>
          <h1>Perguntas frequentes</h1>

          <img src={banner} alt="Banner FAQ Adiante" />
        </div>

        <div className="right">
          <section className="faq">
            <div className="items">
              <div className="item">
                <input type="checkbox" id="item-1" />
                <label htmlFor="item-1">
                  <h4>
                    Como é feito o processo para antecipar duplicatas online?
                  </h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  Ao lançar as notas fiscais eletrônicas na versão XML no site,
                  o sistema realizará a análise em segundos. Caso aprovadas, as
                  suas notas fiscais aparecerão no menu “Operação”, seguindo
                  adiante com o processo. As notas que forem reprovadas serão
                  exibidas no menu “Carteira” e, posteriormente, em “Histórico
                  de Operações”.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-2" />
                <label htmlFor="item-2">
                  <h4>
                    Quanto tempo demora para o dinheiro cair na minha conta?
                  </h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  Selecione as parcelas desejadas e clique em “Aceitar
                  parcelas”. Ao verificar o valor de aquisição, já com o valor
                  descontado da taxa, aperte em “Confirmar” para receber esse
                  valor na sua conta bancária em até 30 minutos.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-3" />
                <label htmlFor="item-3">
                  <h4>Aceita qualquer tipo de nota?</h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  Até o momento, o sistema aceita apenas notas fiscais
                  eletrônicas de produto sacadas contra pessoas físicas e
                  jurídicas.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-4" />
                <label htmlFor="item-4">
                  <h4>Quanto de taxa é cobrado?</h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  O Adiante opera com taxas a partir de 1% ao mês, de acordo com
                  a análise de risco e informações de mercado de seu cliente.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-5" />
                <label htmlFor="item-5">
                  <h4>Há cobrança direta com os meus clientes?</h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  Os boletos estarão disponíveis para download em sua “Carteira”
                  na plataforma, mas o Adiante também enviará a via física para
                  os seus clientes. Caso os títulos não sejam pagos por seus
                  clientes, haverá o envio para cartório em até 7 dias.
                  <br />
                  Três dias após o vencimento do título será gerado um boleto
                  contra a empresa cedente, que terá a obrigação de liquidar
                  estes boletos e, caso o pagamento não seja realizado, será
                  enviado para cartório.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-6" />
                <label htmlFor="item-6">
                  <h4>Posso fazer operações a qualquer horário?</h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  Claro! No Adiante não tem horário para o dinheiro entrar na
                  sua conta! Você poderá fazer a operação a qualquer momento.
                  <br />
                  Caso a sua empresa possua conta no Santander, Bradesco ou
                  Itaú, o crédito será feito no mesmo momento da operação (24
                  horas por dia). Se a sua empresa não possuir conta em nenhum
                  desses bancos, o crédito será enviado durante o horário
                  bancário.
                </div>
              </div>

              <div className="item">
                <input type="checkbox" id="item-7" />
                <label htmlFor="item-7">
                  <h4>Qual conta bancária posso cadastrar?</h4>
                  <span className="chevron">></span>
                </label>
                <div className="text-content">
                  O crédito será enviado somente se a conta bancária cadastrada
                  estiver vinculada ao CNPJ da empresa.
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  );
};

export default FAQ;
