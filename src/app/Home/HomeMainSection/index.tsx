import React from "react";
import { Link } from "react-router-dom";

import MenuItems from "../Menu/MenuItems";

import addy from "../../../assets/images/addy-mascote-adiante.png";
import analiseCredito from "../../../assets/images/main-section/analise-de-credito.svg";
import balaoAddy from "../../../assets/images/main-section/balao-eu-sou-addy.svg";
import clock from "../../../assets/images/main-section/clock.svg";
import dinheiroNaConta from "../../../assets/images/main-section/30-minutos-dinheiro-na-conta.svg";
import trintaMinutos from "../../../assets/images/main-section/30-minutos.svg";

import "./styles.scss";

const HomeMainSection: React.FC = () => {
  return (
    <main className="home-main-section">
      <div className="home-main-section-container">
        <div className="left">
          <MenuItems isHidden={true} />

          <main className="content">
            <div className="top">
              <h1>
                Já pensou em receber <strong>sempre à vista?</strong>
              </h1>

              <p>
                Antecipe recebíveis sem burocracia e obtenha capital de giro
                para a sua empresa.
              </p>
            </div>

            <div className="bottom">
              <div className="images-items">
                <div className="image-item">
                  <img src={clock} alt="15 segundos para análise de crédito" />

                  <span>
                    <strong>15 segundos</strong> para <br />
                    análise de crédito
                  </span>
                </div>

                <span className="divisor"></span>

                <div className="image-item">
                  <img
                    src={trintaMinutos}
                    alt="30 minutos e o dinheiro na sua conta*"
                  />

                  <span>
                    <strong>30 minutos</strong> e o <br />
                    dinheiro na sua conta*
                  </span>
                </div>
              </div>

              <Link to="/cadastro" className="button-flat-primary">
                QUERO ANTECIPAR
              </Link>
            </div>
          </main>
        </div>

        <div className="right">
          <div className="image-wrapper">
            <img src={addy} alt="Mascote Adiante - Addy" className="addy" />

            <img
              src={balaoAddy}
              alt="Olá, eu sou o Addy"
              className="baloon baloon-desktop"
            />
          </div>

          <div className="mobile-version">
            <div className="img-wrapper">
              <img
                src={addy}
                alt="Mascote Adiante - Addy"
                className="addy-mobile"
              />

              <img
                src={balaoAddy}
                alt="Olá, eu sou o Addy"
                className="baloon"
              />
            </div>

            <div className="content">
              <div className="items">
                <div className="item">
                  <div className="top">
                    <img
                      src={analiseCredito}
                      alt="15 segundos para análise de crédito"
                    />

                    <div className="text">
                      <span className="big">15</span>
                      <span>segundos</span>
                    </div>
                  </div>

                  <div className="bottom">para análise de crédito</div>
                </div>

                <div className="divisor"></div>

                <div className="item">
                  <div className="top">
                    <img
                      src={dinheiroNaConta}
                      alt="30 minutos e o dinheiro na sua conta*"
                    />

                    <div className="text">
                      <span className="big">30</span>
                      <span>minutos</span>
                    </div>
                  </div>

                  <div className="bottom">e o dinheiro na sua conta*</div>
                </div>
              </div>

              <Link to="/cadastro" className="button-flat-primary">
                QUERO ANTECIPAR
              </Link>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default HomeMainSection;
