import React from "react";

import "./styles.scss";

interface AnchorProps {
  anchorId: string;
  offset?: number;
}

const Anchor: React.FC<AnchorProps> = props => {
  const { anchorId, offset } = props;

  return (
    <div className="anchor" id={anchorId} style={{ top: `-${offset}px` }}></div>
  );
};

Anchor.defaultProps = {
  offset: 50
};

export default Anchor;
