import React from "react";

import addy from "../../../assets/images/menu-floating/addy.svg";
import facebook from "../../../assets/images/menu-floating/facebook.svg";
import linkedin from "../../../assets/images/menu-floating/linkedin.svg";
import instagram from "../../../assets/images/menu-floating/instagram.svg";
import whatsapp from "../../../assets/images/menu-floating/whatsapp.svg";

import "./styles.scss";
import { Link } from "react-router-dom";

const FloatingSocialLinks: React.FC = () => {
  return (
    <nav className="menu-floating">
      <img src={addy} alt="Ícone do Mascote Addy" />

      <a
        rel="noopener noreferrer"
        target="_blank"
        href="https://www.facebook.com/adianteapp"
      >
        <img src={facebook} alt="link para facebook" />
      </a>

      <a
        rel="noopener noreferrer"
        target="_blank"
        href="https://www.linkedin.com/company/adiante-s-a"
      >
        <img src={linkedin} alt="link para linkedin" />
      </a>

      <a
        rel="noopener noreferrer"
        target="_blank"
        href="https://www.instagram.com/adiante.app/"
      >
        <img src={instagram} alt="link para instagram" />
      </a>

      <Link target="_blank" to="#">
        <img src={whatsapp} alt="link para whatsapp" />
      </Link>
    </nav>
  );
};

export default FloatingSocialLinks;
