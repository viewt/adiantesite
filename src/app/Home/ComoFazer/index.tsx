import React from "react";

import banner from "../../../assets/images/section2/adiante-banner-como-fazer.png";
import cadastre from "../../../assets/images/section2/cadastre-se.svg";
import circles from "../../../assets/images/section2/circles-divisor.svg";
import seuLimite from "../../../assets/images/section2/seu-limite-em-15-segundos.svg";
import solicite from "../../../assets/images/section2/solicite-a-sua-antecipacao.svg";
import dinheiro from "../../../assets/images/section2/dinheiro-em-ate-30-minutos.svg";

import "./styles.scss";
import Anchor from "../Anchor";

const ComoFazer: React.FC = () => {
  return (
    <section className="section2 has-anchor">
      <Anchor anchorId="como-fazer" />

      <div className="container">
        <div className="left">
          <h2 className="dark">Como fazer</h2>

          <img src={banner} alt="Adiante Banner - Como Fazer" />
        </div>

        <div className="left-mobile">
          <p>
            *O crédito será feito no mesmo momento da operação (24 horas por
            dia), caso a sua empresa possua conta no Santander, Bradesco ou
            Itaú.
          </p>

          <div className="img-container">
            <div className="img-wrapper"></div>
          </div>
        </div>

        <div className="right">
          <div className="items">
            <h2 className="title">Como fazer</h2>

            <div className="item">
              <div className="item-left">
                <img src={cadastre} alt="Cadastre-se" />
              </div>
              <div className="item-right">
                <h5>Cadastre-se</h5>
                <p>Não é preciso enviar nenhum documento. Sem burocracia!</p>
              </div>
            </div>

            <div className="divisor">
              <img src={circles} alt="divisor" />
            </div>

            <div className="item">
              <div className="item-left">
                <img src={seuLimite} alt="Seu limite em 15 segundos" />
              </div>
              <div className="item-right">
                <h5>Seu limite em 15 segundos</h5>
                <p>A análise de crédito é rápida, objetiva e funcional.</p>
              </div>
            </div>

            <div className="divisor">
              <img src={circles} alt="divisor" />
            </div>

            <div className="item">
              <div className="item-left">
                <img src={solicite} alt="Solicite a sua antecipação" />
              </div>
              <div className="item-right">
                <h5>Solicite a sua antecipação</h5>
                <p>Envie as suas NFs de produtos e aguarde a liberação.</p>
              </div>
            </div>

            <div className="divisor">
              <img src={circles} alt="divisor" />
            </div>

            <div className="item">
              <div className="item-left">
                <img
                  src={dinheiro}
                  alt="Cadastre-se"
                  className="dinheiro-30-minutos"
                />
              </div>
              <div className="item-right">
                <h5>O dinheiro em até 30 minutos*</h5>
                <p>O valor solicitado cai na sua conta no mesmo dia.</p>
              </div>
            </div>

            <p className="help-text">
              *O crédito será feito no mesmo momento da operação (24 horas por
              dia), caso a sua empresa possua conta no Santander, Bradesco ou
              Itaú.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ComoFazer;
