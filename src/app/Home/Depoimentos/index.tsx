import React from "react";

import aspasMobile from "../../../assets/images/section5/aspas-mobile.svg";
import stars from "../../../assets/images/section5/stars.svg";
import aspas from "../../../assets/images/section5/aspas.svg";

import "./styles.scss";

const Depoimentos = () => {
  return (
    <section className="section5">
      <div className="top">
        <h4>Depoimentos</h4>
        <h1>
          Experiência de quem já adiantou
          <img src={aspasMobile} alt="aspas decoração" className="aspas" />
        </h1>
      </div>

      <div className="container">
        <div className="depoimentos-wrapper">
          <div className="depoimentos">
            <div className="depoimento">
              <strong>Simples, rápido e intuitivo.</strong>

              <p className="depoimento-text">
                “O Adiante tem um sistema simples, rápido e intuitivo.”
              </p>

              <span className="name">Júlio Marques</span>
              <span className="empresa">Base Brands</span>

              <div className="depoimento-footer">
                <img src={stars} alt="avaliação depoimento - estrelas" />
              </div>
            </div>

            <div className="depoimento">
              <strong>Sem burocracia, mais rápido e prático</strong>

              <p className="depoimento-text">
                “É bem simples e sem burocracia, mais rápido e prático para quem
                não possui tempo. Aqui na empresa gostamos muito.”
              </p>

              <span className="name">Stella Campos</span>
              <span className="empresa">SoftLab</span>

              <div className="depoimento-footer">
                <img src={stars} alt="avaliação depoimento - estrelas" />
              </div>
            </div>

            <div className="depoimento">
              <strong>Aprovação rápida e melhor taxa</strong>

              <p className="depoimento-text">
                “Aprovação muito rápida, a melhor taxa que encontramos no
                mercado e o atendimento pontual da equipe Adiante nos
                impressionou positivamente.”
              </p>

              <span className="name">Isabella Felicio</span>
              <span className="empresa">Frutara Brasil</span>

              <div className="depoimento-footer">
                <img src={stars} alt="avaliação depoimento - estrelas" />
              </div>

              <img src={aspas} alt="aspas decoração" className="aspas" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Depoimentos;
