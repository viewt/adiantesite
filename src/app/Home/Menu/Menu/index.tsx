import React, { useState } from "react";
import { Link } from "react-router-dom";

import MenuItems from "../MenuItems";

import addy from "../../../../assets/images/menu/addy.svg";
import facebook from "../../../../assets/images/menu/facebook.svg";
import googlePlay from "../../../../assets/images/footer/google-play.svg";
import instagram from "../../../../assets/images/menu/instagram.svg";
import linkedin from "../../../../assets/images/menu/linkedin.svg";
import logo from "../../../../assets/images/adiante-logo.svg";
import logoWhite from "../../../../assets/images/adiante-logo-white.svg";
import whatsapp from "../../../../assets/images/menu/whatsapp.svg";

import "./styles.scss";

const Menu: React.FC = () => {
  const [menuOpened, setMenuOpened] = useState(false);

  const toggleMenu = () => {
    setMenuOpened(!menuOpened);
  };

  return (
    <React.Fragment>
      <div className="menu">
        <div className="menu-container">
          <MenuItems />

          <div className="right">
            <Link to="/login" className="button-outline-white">
              Sou Cliente
            </Link>

            <Link to="/cadastro" className="button-flat-light-blue">
              Adiante já
            </Link>
          </div>
        </div>
      </div>

      <div className="menu-mobile">
        <input
          type="checkbox"
          id="hamburguer-controller"
          checked={menuOpened}
          onChange={() => {}}
        />

        <div className="menu-mobile-container">
          <Link to="/">
            <img src={logo} alt="adiante logo" />
          </Link>

          <div className="hamburguer-container">
            <label
              className="hamburguer"
              htmlFor="hamburguer-controller"
              role="menu"
              tabIndex={0}
              onClick={toggleMenu}
            >
              <span className="line line1"></span>
              <span className="line line2"></span>
              <span className="line line3"></span>
            </label>
          </div>
        </div>

        <div className="menu-mobile-fixed">
          <div className="content">
            <div className="top">
              <img src={logoWhite} alt="adiante logo" />
            </div>

            <nav className="items">
              <ul className="items-container">
                <li className="item-container with-button">
                  <Link
                    to="/login"
                    className="button-outline-white"
                    onClick={toggleMenu}
                  >
                    Sou Cliente
                  </Link>
                </li>

                <li className="item-container with-button">
                  <Link
                    to="/cadastro"
                    className="button-flat-light-blue dark-text"
                    onClick={toggleMenu}
                  >
                    Adiante já
                  </Link>
                </li>

                <li className="item-container">
                  <a className="item" href="#como-fazer" onClick={toggleMenu}>
                    Como fazer
                  </a>
                </li>

                <li className="item-container">
                  <a className="item" href="#vantagens" onClick={toggleMenu}>
                    Vantagens
                  </a>
                </li>

                <li className="item-container">
                  <a className="item" href="#quem-somos" onClick={toggleMenu}>
                    Quem somos
                  </a>
                </li>

                <li className="item-container">
                  <a className="item" href="#atendimento" onClick={toggleMenu}>
                    Sacado
                  </a>
                </li>
              </ul>
            </nav>

            <div className="menu-footer">
              <div className="menu-social-medias">
                <h4>Nossas Redes Sociais</h4>

                <nav className="menu-icons">
                  <img src={addy} alt="Ícone do Mascote Addy" />

                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href="https://www.facebook.com/adianteapp"
                  >
                    <img src={facebook} alt="link para facebook" />
                  </a>

                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href="https://www.linkedin.com/company/adiante-s-a"
                  >
                    <img src={linkedin} alt="link para linkedin" />
                  </a>

                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href="https://www.instagram.com/adiante.app/"
                  >
                    <img src={instagram} alt="link para instagram" />
                  </a>

                  <Link target="_blank" to="#">
                    <img src={whatsapp} alt="link para whatsapp" />
                  </Link>
                </nav>
              </div>
              <div className="app-download">
                <h4>Disponível para download</h4>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://play.google.com/store/apps/details?id=com.adiantesa&hl=pt"
                >
                  <img
                    src={googlePlay}
                    alt="Google Play Botão"
                    className="google-play"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Menu;
