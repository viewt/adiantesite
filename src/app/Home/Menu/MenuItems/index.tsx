import React from "react";
import { Link } from "react-router-dom";

import logo from "../../../../assets/images/adiante-logo.svg";

import "./styles.scss";

interface MenuItemsProps {
  isHidden?: boolean;
}

const MenuItems: React.FC<MenuItemsProps> = props => {
  const { isHidden } = props;

  return (
    <div className={`menu-layout ${isHidden ? "menu-hidden" : ""}`}>
      <Link to="/">
        <img src={logo} alt="adiante logo" />
      </Link>

      <nav className="items">
        <ul className="items-container">
          <li className="item-container">
            <a className="item" href="#como-fazer">
              Como fazer
            </a>
          </li>

          <li className="item-container">
            <a className="item" href="#vantagens">
              Vantagens
            </a>
          </li>

          <li className="item-container">
            <a className="item" href="#quem-somos">
              Quem somos
            </a>
          </li>

          <li className="item-container">
            <a className="item" href="#sacado">
              Sacado
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default MenuItems;
