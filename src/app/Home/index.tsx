import React from "react";

import ComoFazer from "./ComoFazer";
import Depoimentos from "./Depoimentos";
import FAQ from "./FAQ";
import FloatingSocialLinks from "./FloatingSocialLinks";
import Footer from "./Footer";
import HomeMainSection from "./HomeMainSection";
import Menu from "./Menu/Menu";
import NewsAdiante from "./NewsAdiante";
import QuemSomos from "./QuemSomos";
import Vantagens from "./Vantagens";

import "./styles.scss";

const Home: React.FC = () => {
  return (
    <div className="home">
      <FloatingSocialLinks />
      <Menu />
      <HomeMainSection />
      <ComoFazer />
      <Vantagens />
      <QuemSomos />
      <Depoimentos />
      <FAQ />
      <NewsAdiante />
      <Footer />
    </div>
  );
};

export default Home;
