import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#0D73E8"
    },
    secondary: {
      main: "#00caf9"
    },
    error: {
      main: "#e54141"
    }
  }
});

export default theme;
