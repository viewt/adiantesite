import React from "react";

import addy from "../../../../assets/images/addy-mascote-adiante.png";
import balao from "../../../../assets/images/main-section/balao-eu-sou-addy.svg";

import "./styles.scss";

interface AddyProps {
  baloonClasses?: string;
  classes?: string;
  hasAnimation?: boolean;
  hasBaloon?: boolean;
  width?: string;
  wrapperClasses?: string;
}

const Addy: React.FC<AddyProps> = props => {
  const {
    baloonClasses,
    classes,
    hasAnimation,
    hasBaloon,
    width,
    wrapperClasses
  } = props;

  return (
    <div className={`image-wrapper ${wrapperClasses ? wrapperClasses : ""}`}>
      <img
        src={addy}
        alt="Mascote Adiante - Addy"
        className={`addyComponent ${classes ? classes : ""} ${
          hasAnimation ? "animate" : ""
        }`}
        width={width}
      />

      {hasBaloon && (
        <img
          src={balao}
          alt="Olá, eu sou o Addy"
          className={`baloonComponent ${baloonClasses ? baloonClasses : ""} ${
            hasAnimation ? "animate" : ""
          }`}
        />
      )}
    </div>
  );
};

export default Addy;
