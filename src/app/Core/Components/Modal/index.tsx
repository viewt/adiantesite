import React, { useRef } from "react";
import { createPortal } from "react-dom";

import "./styles.scss";

interface ModalProps {
  onClick?: () => any;
}

const Modal: React.FC<ModalProps> = props => {
  const contentRef = useRef(null);
  const { children, onClick } = props;

  return createPortal(
    <div className="modal" onClick={onClick}>
      <div className="content" ref={contentRef}>
        {children}
      </div>
    </div>,
    // @ts-ignore
    document.getElementById("modal_root")
  );
};

export default Modal;
