import React from "react";
import { createPortal } from "react-dom";

import CircularProgress from "@material-ui/core/CircularProgress";

import "./styles.scss";

interface LoadingProps {
  classes?: object;
  color?: "primary" | "secondary" | "inherit";
  disableShrink?: boolean;
  isLoading: boolean;
  size?: number | string;
  thickness?: number;
  value?: number;
  variant?: "determinate" | "indeterminate" | "static";
}

const Loading: React.FC<LoadingProps> = props => {
  const {
    classes,
    color,
    disableShrink,
    isLoading,
    size,
    thickness,
    value,
    variant
  } = props;

  return createPortal(
    <div>
      {isLoading && (
        <div className="loading-indicator-wrapper">
          <CircularProgress
            classes={classes}
            color={color ? color : "primary"}
            disableShrink={disableShrink}
            size={size ? size : 60}
            thickness={thickness ? thickness : 3.8}
            value={value}
            variant={variant}
          />
        </div>
      )}
    </div>,
    // @ts-ignore
    document.getElementById("loading_indicator")
  );
};

export default Loading;
