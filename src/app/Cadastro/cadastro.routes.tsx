import { GuardedRouteProps } from "react-router-guards";

import CadastroInicio from "./CadastroInicio";
import ValidarEmail from "./ValidarEmail";
import AnalisandoCadastro from "./AnalisandoCadastro";
import Password from "./Password";

import {
  passwordGuard,
  requireFullRegistrationGuard,
  validarEmailGuard
} from "./cadastro.guards";

const cadastroRoutes: GuardedRouteProps[] = [
  {
    path: "",
    component: CadastroInicio,
    exact: true
  },
  {
    path: "validar-email",
    component: ValidarEmail,
    guards: [validarEmailGuard]
  },
  {
    path: "escolher-senha",
    component: Password,
    guards: [passwordGuard]
  },
  {
    path: "analisando-cadastro",
    component: AnalisandoCadastro,
    guards: [requireFullRegistrationGuard]
  }
];

export default cadastroRoutes;
