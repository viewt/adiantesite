import React, { MouseEvent, useRef } from "react";
import Helmet from "react-helmet";

import imagem from "../../../../assets/images/cadastro/addy-email-enviado-com-sucesso.svg";

import "./styles.scss";

interface EmailEnviadoProps {
  onClick: Function;
}

const EmailEnviado: React.FC<EmailEnviadoProps> = props => {
  const { onClick } = props;

  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const ref3 = useRef(null);

  const handleOnClick = (event: MouseEvent<any>) => {
    const refs = [ref1.current, ref2.current, ref3.current];

    const found = refs.find(ref => ref === event.target);

    if (!found) {
      return false;
    }

    onClick();
  };

  return (
    <div className="email-enviado-wrapper" ref={ref1} onClick={handleOnClick}>
      <Helmet title="Adiante S/A - Cadastro E-mail Enviado" />

      <div className="left" ref={ref2}></div>
      <div className="right" ref={ref3}>
        <div className="email-enviado">
          <div className="top">
            <div className="button-close" onClick={() => onClick()}>
              <span className="line line1"></span>
              <span className="line line2"></span>
            </div>
          </div>

          <img src={imagem} alt="E-mail enviado com sucesso" />

          <h1>
            E-mail enviado <br /> <strong>com sucesso</strong>
          </h1>
        </div>
      </div>
    </div>
  );
};

export default EmailEnviado;
