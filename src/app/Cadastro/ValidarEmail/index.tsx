import React, { useState } from "react";
import {
  Link,
  Redirect,
  Route,
  RouteComponentProps,
  Switch
} from "react-router-dom";
import useForm from "react-hook-form";
import Helmet from "react-helmet";

import { Button } from "@material-ui/core";
import ReplayIcon from "@material-ui/icons/Replay";

import axios from "axios";

import CustomSnackbar from "../../Core/Components/CustomSnackbar";
import EmailEnviado from "./EmailEnviado";
import Loading from "../../Core/Components/Loading";
import Modal from "../../Core/Components/Modal";
import ReenviarCodigo from "./ReenviarCodigo";

import "./styles.scss";

type FormData = {
  code: string;
};

const ValidarEmail: React.FC<RouteComponentProps> = props => {
  const { history, match } = props;

  const { errors, register, handleSubmit } = useForm<FormData>();
  const [loading, setLoading] = useState(false);
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarStyle, setSnackbarStyle] = useState("success");
  const [toNextStep, setToNextStep] = useState(false);
  const [code, setCode] = useState("");

  const closeModal = (callback?: Function) => {
    history.push(match!.url);

    if (callback) {
      callback();
    }
  };

  const openSnackbar = (style: "success" | "error", message: string) => {
    setSnackbarStyle(style);
    setSnackbarMessage(message);
    setSnackbarOpened(true);
  };

  const enviarNovoCodigo = () => {
    const cadastroLocalStorage = localStorage.getItem("cadastro");

    const { cnpj, email } = cadastroLocalStorage
      ? JSON.parse(cadastroLocalStorage)
      : { cnpj: "", email: "" };

    setLoading(true);

    // TODO: colocar chamada e tratamento para enviar novo código
    axios
      .post<any>("http://url/cadastro-reenviar-codigo", {
        email,
        cnpj
      })
      .then(response => {
        if (response.data.success) {
          openSnackbar("success", "Código enviado");
        } else {
          openSnackbar("error", "Tente novamente");
        }
      })
      .catch(response => {
        openSnackbar("error", "Tente novamente");
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onSubmit = handleSubmit(data => {
    const { code } = data;

    setLoading(true);

    // TODO: colocar chamada e tratamento de validação do código
    axios
      .post<any>("http://url/cadastro-validar-email", {
        code
      })
      .then(response => {
        if (response.data.success) {
          localStorage.setItem("validarEmailDone", "true");

          setToNextStep(true);
        } else {
          openSnackbar(
            "error",
            "Não foi possível validar o código. Tente novamente."
          );
        }
      })
      .catch(response => {
        openSnackbar(
          "error",
          "Não foi possível validar o código. Tente novamente."
        );
      })
      .finally(() => {
        setLoading(false);
      });
  });

  const handleChangeCode = (e: any) => {
    const re = /^[0-9\b]+$/;
    if (e.target.value === "" || re.test(e.target.value)) {
      setCode(e.target.value);
    }
  };

  if (toNextStep) {
    return <Redirect to={`/cadastro/escolher-senha`} />;
  }

  return (
    <div className="validar-email" onSubmit={onSubmit}>
      <Helmet>
        <title>Validar E-mail - Cadastro</title>
      </Helmet>

      <Loading isLoading={loading} />

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle={snackbarStyle}
      />

      <div className="top">
        <h1>Valide o seu e-mail</h1>
        <p>Enviamos um código de verificação para o e-mail cadastrado.</p>
      </div>

      <form className="form-validar-email">
        <div className="code-wrapper">
          <h4>Digite o código aqui</h4>

          <div className="input-code">
            <span className="number-box"></span>
            <span className="number-box"></span>
            <span className="number-box"></span>
            <span className="number-box"></span>
            <span className="number-box"></span>
            <span className="number-box"></span>

            <input
              type="text"
              name="code"
              maxLength={6}
              value={code}
              onChange={handleChangeCode}
              ref={register({ required: true, minLength: 6, maxLength: 6 })}
            />

            <textarea
              className={`${
                code.length === 3 || code.length === 6 ? "hide-cursor" : ""
              }`}
              name="code"
              maxLength={6}
              value={code}
              onChange={handleChangeCode}
              ref={register({ required: true, minLength: 6, maxLength: 6 })}
              rows={2}
            ></textarea>
          </div>

          {errors.code ? (
            <span className="error-text">Código inválido</span>
          ) : (
            ""
          )}

          <div className="code-bottom">
            <Button
              size="small"
              color="primary"
              className="button-link-with-icon"
              onClick={enviarNovoCodigo}
            >
              <ReplayIcon className="replay-icon" />
              Envie novo código
            </Button>

            <Link to={`${match!.url}/reenviar-codigo`}>
              <Button size="small" color="primary" className="button-link">
                Não recebeu o código?
              </Button>
            </Link>
          </div>
        </div>

        <Button
          className="button-outline"
          variant="outlined"
          size="large"
          color="primary"
          type="submit"
        >
          AVANÇAR
        </Button>
      </form>

      <Switch>
        <Route
          path={`${match!.url}/reenviar-codigo`}
          render={() => (
            <Modal>
              {/*
                // @ts-ignore */}
              <ReenviarCodigo onClick={closeModal} />
            </Modal>
          )}
          exact={true}
        />

        <Route
          path={`${match!.url}/reenviar-codigo/email-enviado`}
          render={() => (
            <Modal>
              <EmailEnviado onClick={closeModal} />
            </Modal>
          )}
        />
      </Switch>
    </div>
  );
};

export default ValidarEmail;
