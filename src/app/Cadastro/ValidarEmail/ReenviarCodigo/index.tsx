import React, { MouseEvent, useRef, useState } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import Helmet from "react-helmet";
import useForm from "react-hook-form";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import axios from "axios";

import CustomSnackbar from "../../../Core/Components/CustomSnackbar";
import Loading from "../../../Core/Components/Loading";
import MaskCnpj from "../../../Core/Masks/cnpjMask";
import { regexCpfCnpj, regexEmail } from "../../../Core/Utils/regex";

import "./styles.scss";

type FormData = {
  cnpj: string;
  email: string;
};

interface ReenviarCodigoProps {
  onClick: Function;
}

const ReenviarCodigo: React.FC<ReenviarCodigoProps &
  RouteComponentProps> = props => {
  const { history, match, onClick } = props;

  const [loading, setLoading] = useState(false);
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarStyle, setSnackbarStyle] = useState("success");
  const { register, handleSubmit, errors } = useForm<FormData>();

  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const ref3 = useRef(null);

  const cadastroLocalStorage = localStorage.getItem("cadastro");

  const {
    cnpj: cnpjLocalStorage,
    email: emailLocalStorage
  } = cadastroLocalStorage
    ? JSON.parse(cadastroLocalStorage)
    : { cnpj: "", email: "" };

  const openSnackbar = (style: "success" | "error", message: string) => {
    setSnackbarStyle(style);
    setSnackbarMessage(message);
    setSnackbarOpened(true);
  };

  const handleOnClick = (event: MouseEvent<any>) => {
    const refs = [ref1.current, ref2.current, ref3.current];

    const found = refs.find(ref => ref === event.target);

    if (!found) {
      return false;
    }

    onClick();
  };

  const onSubmit = handleSubmit(data => {
    const { cnpj, email } = data;

    setLoading(true);

    // Caso os dados estejam corretos, só reenviar o código. Se não, fazer um novo cadastro.
    // TODO
    if (cnpj === cnpjLocalStorage && email === emailLocalStorage) {
      axios
        .post<any>("http://url/cadastro-reenviar-codigo", {
          email,
          cnpj
        })
        .then(response => {
          if (response.data.success) {
            history.push(`${match!.url}/email-enviado`);
          } else {
            openSnackbar("error", "Tente novamente");
          }
        })
        .catch(response => {
          openSnackbar("error", "Tente novamente");
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      axios
        .post<any>("http://url/cadastro", {
          cnpj,
          email
        })
        .then(response => {
          if (response.data.success) {
            localStorage.setItem("cadastro", JSON.stringify({ email, cnpj }));

            history.push(`${match!.url}/email-enviado`);
          } else {
            openSnackbar(
              "error",
              "Não foi possível realizar seu cadastro. Tente novamente."
            );
          }
        })
        .catch(response => {
          openSnackbar(
            "error",
            "Não foi possível realizar seu cadastro. Tente novamente."
          );
        })
        .finally(() => {
          setLoading(false);
        });
    }
  });

  return (
    <div className="reenviar-codigo-wrapper" ref={ref1} onClick={handleOnClick}>
      <Helmet title="Adiante S/A - Cadastro Reenviar Código" />

      <Loading isLoading={loading} />

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle={snackbarStyle}
      />

      <div className="left" ref={ref2}></div>

      <div className="right" ref={ref3}>
        <div className="reenviar-codigo">
          <div className="top">
            <div className="button-close" onClick={() => onClick()}>
              <span className="line line1"></span>
              <span className="line line2"></span>
            </div>
          </div>

          <h2>Solicitação de código de verificação</h2>

          <p>
            Confirme os seus dados e, caso haja divergências, altere as
            informações abaixo para receber um novo código de acesso.
          </p>

          <form onSubmit={onSubmit}>
            <div className="inputs">
              <TextField
                defaultValue={cnpjLocalStorage}
                error={errors.cnpj ? !!errors.cnpj.message : false}
                helperText={errors.cnpj ? errors.cnpj.message : ""}
                inputRef={register({
                  required: "Campo obrigatório",
                  pattern: {
                    value: regexCpfCnpj,
                    message: "CNPJ inválido"
                  }
                })}
                name="cnpj"
                label="CNPJ"
                id="CNPJ"
                InputProps={{
                  inputComponent: MaskCnpj
                }}
              />

              <TextField
                defaultValue={emailLocalStorage}
                error={errors.email ? !!errors.email.message : false}
                helperText={errors.email ? errors.email.message : ""}
                inputRef={register({
                  required: "Campo obrigatório",
                  pattern: {
                    value: regexEmail,
                    message: "E-mail inválido"
                  }
                })}
                name="email"
                label="E-mail corporativo"
                type="email"
                id="Email"
              />
            </div>

            <div className="buttons">
              <Button
                className="button-outline small"
                variant="outlined"
                size="large"
                color="primary"
                onClick={() => {
                  onClick();
                }}
              >
                CANCELAR
              </Button>

              <Button
                className="button-flat small"
                variant="outlined"
                size="large"
                color="primary"
                type="submit"
              >
                REENVIAR CÓDIGO
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default withRouter(ReenviarCodigo);
