import React from "react";
import { RouteChildrenProps, Switch } from "react-router";
import { Link } from "react-router-dom";
import { GuardProvider, GuardedRoute } from "react-router-guards";
import Helmet from "react-helmet";

import cadastroRoutes from "./cadastro.routes";

import logoWhite from "../../assets/images/adiante-logo-white.svg";

import "./styles.scss";

function Cadastro(props: RouteChildrenProps) {
  return (
    <div className="cadastro">
      <Helmet title="Adiante S/A - Cadastro" />

      <div className="left">
        <img src={logoWhite} alt="Adiante Logo" />
      </div>

      <main className="right">
        <div className="top">
          <Link to="/" className="button-close">
            <span className="line line1"></span>
            <span className="line line2"></span>
          </Link>
        </div>

        <div className="content">
          <Switch>
            {/*
            // @ts-ignore */}
            <GuardProvider loading="">
              {cadastroRoutes.map(({ component, exact, guards, path }, i) => (
                <GuardedRoute
                  path={`${props.match!.url}/${path}`}
                  key={i}
                  exact={exact}
                  component={component}
                  guards={guards}
                />
              ))}
            </GuardProvider>
          </Switch>
        </div>
      </main>
    </div>
  );
}

export default Cadastro;
