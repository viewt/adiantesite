import { GuardFunction } from "react-router-guards";

export const validarEmailGuard: GuardFunction = (to, from, next) => {
  const canAccess = localStorage.getItem("cadastroInicioDone");

  if (!canAccess) {
    next.redirect("/cadastro");
  }

  next();
};

export const passwordGuard: GuardFunction = (to, from, next) => {
  const canAccess =
    localStorage.getItem("cadastroInicioDone") &&
    localStorage.getItem("validarEmailDone");

  if (!canAccess) {
    next.redirect("/cadastro");
  }

  next();
};

export const requireFullRegistrationGuard: GuardFunction = (to, from, next) => {
  const fullRegistration =
    localStorage.getItem("cadastroInicioDone") &&
    localStorage.getItem("validarEmailDone") &&
    localStorage.getItem("passwordDone");

  if (!fullRegistration) {
    next.redirect("/cadastro");
  }

  next();
};
