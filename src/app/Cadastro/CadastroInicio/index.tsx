import React, { useState } from 'react';
import { Redirect, RouteChildrenProps } from 'react-router-dom';
import useForm from 'react-hook-form';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

import CustomSnackbar from '../../Core/Components/CustomSnackbar';
import Loading from '../../Core/Components/Loading';
import MaskCnpj from '../../Core/Masks/cnpjMask';
import { regexCpfCnpj, regexEmail } from '../../Core/Utils/regex';

import './styles.scss';

type FormData = {
  cnpj: string;
  email: string;
};

const CadastroInicio: React.FC<RouteChildrenProps> = props => {
  const [loading, setLoading] = useState(false);
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [toNextStep, setToNextStep] = useState(false);
  const { register, handleSubmit, errors } = useForm<FormData>();

  const cadastroLocalStorage = localStorage.getItem('cadastro');

  let cnpjLocalStorage = '';
  let emailLocalStorage = '';

  if (cadastroLocalStorage) {
    const cadastro = JSON.parse(cadastroLocalStorage);

    cnpjLocalStorage = cadastro.cnpj;
    emailLocalStorage = cadastro.email;
  }

  const onSubmit = handleSubmit(data => {
    const { cnpj, email } = data;

    localStorage.setItem('cadastro', JSON.stringify({ email, cnpj }));

    setLoading(true);

    // TODO: colocar chamada e tratamento
    axios
      .post<any>('http://url/cadastro', {
        cnpj,
        email
      })
      .then(response => {
        if (response.data.success) {
          localStorage.setItem('cadastroInicioDone', 'true');

          setToNextStep(true);
        }
      })
      .catch(response => {
        console.log('cadastro error', response);

        setSnackbarMessage(
          'Não foi possível realizar seu cadastro. Tente novamente.'
        );
        setSnackbarOpened(true);
      })
      .finally(() => {
        setLoading(false);
      });
  });

  if (toNextStep) {
    return <Redirect to={`${props.match!.url}/validar-email`} />;
  }

  return (
    <div className="cadastro-inicio">
      <Loading isLoading={loading} />

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle="error"
      />

      <div className="top">
        <h1>Vamos começar!</h1>

        <div className="requisitos">
          <strong>Requisitos mínimos</strong>
          <ul>
            <li>Empresa com, no mínimo, 1 ano de atuação</li>
            <li>Trabalhar apenas com a emissão de NFs de produtos</li>
          </ul>
        </div>
      </div>

      <form onSubmit={onSubmit}>
        <p>
          <strong>Insira os dados abaixo</strong> para criar a sua conta:
        </p>

        <div className="inputs">
          <TextField
            error={errors.cnpj ? !!errors.cnpj.message : false}
            helperText={errors.cnpj ? errors.cnpj.message : ''}
            inputRef={register({
              required: 'Campo obrigatório',
              pattern: {
                value: regexCpfCnpj,
                message: 'CNPJ inválido'
              }
            })}
            name="cnpj"
            label="CNPJ"
            id="CNPJ"
            InputProps={{ inputComponent: MaskCnpj }}
            defaultValue={cnpjLocalStorage}
          />

          <TextField
            error={errors.email ? !!errors.email.message : false}
            helperText={errors.email ? errors.email.message : ''}
            inputRef={register({
              required: 'Campo obrigatório',
              pattern: {
                value: regexEmail,
                message: 'E-mail inválido'
              }
            })}
            name="email"
            label="E-mail corporativo"
            type="email"
            id="Email"
            defaultValue={emailLocalStorage}
          />
        </div>

        <Button
          className="button-outline"
          variant="outlined"
          size="large"
          color="primary"
          type="submit"
        >
          AVANÇAR
        </Button>
      </form>
    </div>
  );
};

export default CadastroInicio;
