import React, { useState } from "react";
import { Redirect, RouteChildrenProps } from "react-router-dom";
import Helmet from "react-helmet";
import useForm from "react-hook-form";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import axios from "axios";

import CustomSnackbar from "../../Core/Components/CustomSnackbar";
import Loading from "../../Core/Components/Loading";

import "./styles.scss";

type FormData = {
  password: string;
  confirmPassword: string;
};

const Password: React.FC<RouteChildrenProps> = props => {
  const [loading, setLoading] = useState(false);
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarStyle, setSnackbarStyle] = useState("success");
  const [toNextStep, setToNextStep] = useState(false);
  const { register, handleSubmit, errors } = useForm<FormData>();

  const openSnackbar = (style: "success" | "error", message: string) => {
    setSnackbarStyle(style);
    setSnackbarMessage(message);
    setSnackbarOpened(true);
  };

  const onSubmit = handleSubmit(data => {
    const { confirmPassword, password } = data;

    if (password !== confirmPassword) {
      openSnackbar("error", "Senhas não conferem");

      return;
    }

    setLoading(true);

    // TODO: colocar chamada e tratamento
    axios
      .post<any>("http://url/password", {
        password
      })
      .then(response => {
        if (response.data.success) {
          localStorage.setItem("passwordDone", "true");

          setToNextStep(true);
        } else {
          openSnackbar(
            "error",
            "Não foi possível realizar seu cadastro. Tente novamente."
          );
        }
      })
      .catch(response => {
        openSnackbar(
          "error",
          "Não foi possível realizar seu cadastro. Tente novamente."
        );
      })
      .finally(() => {
        setLoading(false);
      });
  });

  if (toNextStep) {
    return <Redirect to={`/cadastro/analisando-cadastro`} />;
  }

  return (
    <div className="password-wrapper">
      <Helmet title="Adiante S/A - Cadastro Escolher Senha" />

      <Loading isLoading={loading} />

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle={snackbarStyle}
      />

      <div className="top">
        <h1>Escolha uma senha</h1>

        <p>Utilize apenas números. São necessários 6 dígitos.</p>
      </div>

      <form onSubmit={onSubmit}>
        <div className="inputs">
          <TextField
            error={errors.password ? !!errors.password : false}
            helperText={
              errors.password
                ? errors.password.message
                  ? errors.password.message
                  : "Mínimo de 6 caracteres"
                : ""
            }
            inputRef={register({
              required: "Campo obrigatório",
              minLength: 6
            })}
            name="password"
            label="Criar uma senha"
            id="Password"
            type="password"
          />

          <TextField
            error={errors.confirmPassword ? !!errors.confirmPassword : false}
            helperText={
              errors.confirmPassword
                ? errors.confirmPassword.message
                  ? errors.confirmPassword.message
                  : "Mínimo de 6 caracteres"
                : ""
            }
            inputRef={register({
              required: "Campo obrigatório",
              minLength: 6
            })}
            name="confirmPassword"
            label="Confirmar senha"
            id="ConfirmPassword"
            type="password"
          />
        </div>

        <Button
          className="button-outline"
          variant="outlined"
          size="large"
          color="primary"
          type="submit"
        >
          AVANÇAR
        </Button>
      </form>
    </div>
  );
};

export default Password;
