import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import Helmet from "react-helmet";

import axios from "axios";

import Addy from "../../Core/Components/Addy";
import CustomSnackbar from "../../Core/Components/CustomSnackbar";

import "./styles.scss";

function AnalisandoCadastro() {
  const [snackbarOpened, setSnackbarOpened] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarStyle, setSnackbarStyle] = useState("success");
  const [toNextStep, setToNextStep] = useState(false);

  useEffect(() => {
    localStorage.removeItem("cadastroInicioDone");
    localStorage.removeItem("validarEmailDone");
    localStorage.removeItem("passwordDone");
  });

  useEffect(() => {
    // TODO: colocar chamada e tratamento de validação do código
    const data: any = {};

    axios
      .post<any>("http://url/analisar-cadastro", data)
      .then(response => {
        if (response.data.success) {
          localStorage.removeItem("cadastro");

          setToNextStep(true);
        } else {
          openSnackbar("error", "Não foi possível analisar o cadastro");
        }
      })
      .catch(response => {
        openSnackbar("error", "Não foi possível analisar o cadastro");
      });
  }, []);

  const openSnackbar = (style: "success" | "error", message: string) => {
    setSnackbarStyle(style);
    setSnackbarMessage(message);
    setSnackbarOpened(true);
  };

  if (toNextStep) {
    // TODO: colocar redirecionamento correto
    return <Redirect to={`/nao-sei-pra-onde-vai`} />;
  }

  return (
    <div className="analisando-cadastro">
      <Helmet title="Adiante S/A - Cadastro Analisando">
        <script>
          {`
            fbq('track', 'CompleteRegistration');
          `}
        </script>
      </Helmet>

      <CustomSnackbar
        message={snackbarMessage}
        opened={snackbarOpened}
        setOpened={setSnackbarOpened}
        snacbkarStyle={snackbarStyle}
      />

      <Addy hasAnimation width="250px" classes="addy-analisando-cadastro" />

      <h1>Aguarde alguns</h1>

      <span className="ellipsis-wrapper">
        <h1>segundos</h1>

        <div className="ellipsis">
          <span className="circle"></span>
          <span className="circle"></span>
          <span className="circle"></span>
        </div>
      </span>

      <p>Estamos analisando o seu cadastro!</p>
    </div>
  );
}

export default AnalisandoCadastro;
