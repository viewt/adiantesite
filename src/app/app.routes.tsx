import { RouteProps } from 'react-router';

import Cadastro from './Cadastro';
import Home from './Home';
import Login from './Login';

const appRoutes: RouteProps[] = [
  {
    path: '/',
    component: Home,
    exact: true
  },
  {
    path: '/cadastro',
    component: Cadastro
  },
  {
    path: '/hidden-login',
    component: Login
  }
];

export default appRoutes;
