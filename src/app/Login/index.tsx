import React from 'react';

import backspace from '../../assets/icons/backspace.svg';

import './styles.scss';

const Login: React.FC = () => {
  return (
    <div className="login-container">
      <div className="left"></div>

      <div className="right">
        <div className="content">
          <div className="header">
            <h1>Olá, seja bem vindo!</h1>
            <h2>Insira seus dados para realizar o acesso</h2>
          </div>

          <div className="inputs">
            <div className="input-float-label-wrapper">
              <div className="input-float-label-container">
                <input type="text" placeholder="" required />

                <label className="float-label">Insira seu e-mail ou CNPJ</label>

                <span className="bottom-line"></span>
              </div>

              <span className="error">Erro teste</span>
            </div>

            <div className="input-float-label-wrapper">
              <div className="input-float-label-container has-icon">
                <input type="password" placeholder="" required />

                <label className="float-label">Senha</label>

                <button className="clear-password">
                  <img src={backspace} alt="backspace" />
                </button>

                <span className="bottom-line"></span>
              </div>

              <span className="error">Erro teste</span>
            </div>
          </div>

          <div className="passwords-container">
            <button className="button-numbers">5 ou 2 ou 4</button>
            <button className="button-numbers">3 ou 0 ou 6</button>
            <button className="button-numbers">1 ou 7 ou 3</button>
            <button className="button-numbers">6 ou 9 ou 1</button>
            <button className="button-numbers">4 ou 8 ou 5</button>
          </div>

          <div className="buttons">
            <button className="button-outline">Login</button>

            <button className="button-clear">Esqueci minha senha</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
