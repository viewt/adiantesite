import React from "react";
import ReactDOM from "react-dom";
import TagManager, { TagManagerArgs } from "react-gtm-module";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

const cadastroLocalStorage = JSON.parse(
  localStorage.getItem("cadastro") || '{"email":"", "cnpj":""}'
);

const { cnpj, email } = cadastroLocalStorage;

const tagManagerArgs: TagManagerArgs = {
  gtmId: "GTM-MTZ43SQ",
  dataLayer: {
    cnpj,
    email
  }
};
TagManager.initialize(tagManagerArgs);

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
